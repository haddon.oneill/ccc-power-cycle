#!/usr/bin/env bash
# ccc-power-cycle-test
#
# Runs a test that checks the relaibility of CCC reboots and power cycles.
# A reboot is a software commanded restart within linux
# A power cycle is a remotely triggered loss and regain of power via a
# network controlled relay.
#
#   ccc-power-cycle-test [options]
#
# Options:
#      -c        IP address of the core
#      -r        IP address of network connected relay
#      -f        CSV file with test cases
#

declare target_ipaddr=""
declare relay_ipaddr=""
declare file_name=""
declare total_tests=0
declare passed_tests=0
declare failed_tests=0

usage() {                                      # Function: Print a help message.
  echo "Usage: $0 [-t (t)arget ccc ] [ -r network (r)elay ] [ -f csv (f)ile ]" 1>&2
}

exit_abnormal() {                              # Function: Exit with error.
  usage
  exit 1
}

while getopts ":r:t:f:" options; do              # Loop: Get the next option;
                                               # use silent error checking;
                                               # options n and t take arguments.
  case "${options}" in                         #
    r)                                         # If the option is r,
      relay_ipaddr=${OPTARG}                   # set $relay_ipaddr to specified value.
      ;;
    t)                                         # If the option is t,
      target_ipaddr=${OPTARG}                  # Set $target_ipaddr to specified value.
      ;;
    f)                                         # If the option is f,
      file_name=${OPTARG}                      # Set file_name to specified value.
      ;;
    :)                                         # If expected argument omitted:
      echo "Error: -${OPTARG} requires an argument."
      exit_abnormal                            # Exit abnormally.
      ;;
    *)                                         # If unknown (any other) option:
      exit_abnormal                            # Exit abnormally.
      ;;
  esac
done

check_file()
{
  [ ! -f ${1} ] && { echo "${1} file not found"; exit 99; }
}

run_test()
{
    local test_file
    test_file="${1}"

    printf -- "power cycle test record header : "
    printf -- "%s, " \
      "count" \
      "result" \
      "type" \
      "delay time" \
      "boot time" \
      "passed count" \
      "failed count"
    printf -- "\n"

    local IFS=','
    test_start_time=$(date +%s)
    while read -r test_type test_time; do
        #   run test using first field for type, second for wait delay
        run_one_iter "${test_type}" "${test_time}" || break
    done < "${test_file}"
    test_end_time=$(date +%s)

    total_test_time=$((test_end_time - test_start_time))

    printf -- "power cycle end of test record header : "
    printf -- "%s, " \
      "count" \
      "passed" \
      "failed" \
      "total time" \
      "test file"
    printf -- "\n"

    printf -- "power cycle end of test record : "
    printf -- "%s, " \
      "${total_tests}" \
      "${passed_tests}" \
      "${failed_tests}" \
      "$(date --date="@${total_test_time}" +%T)" \
      "${test_file}"
    printf -- "\n"
}

cycle_command_failed()
{
  local type="${1}"

  printf -- "Command %s failed while trying to reboot target. Exiting testing\n" ${type}
  return 1
}

failed_test_recovery()
{
  printf -- "Attempting to recover from failed test.\n"
  power_cycle_board
  wait_for_board_to_boot && return
  printf -- "Terminating test due to failed recovery after failed test.\n"
  return 1
}

run_one_iter()
{
    local test_type="${1}"
    local test_time="${2}"

    #waits for specified time
    sleep "${test_time}"

    #inc total test count
    ((total_tests++))

    local cycle_command
    case $test_type in
        reboot)
            cycle_command="reboot_board"
            ;;
        powercycle)
            cycle_command="power_cycle_board"
            ;;
        *)
            printf -- "Unknown test type\n" #how should I handle this?
            exit 1
            ;;
    esac

    local cycle_start_time
    cycle_start_time=$(date +%s)

    ${cycle_command} || cycle_command_failed "${cycle_command}" || return

    local test_result="failed"
    wait_for_board_to_boot && test_result="passed"

    case "${test_result}" in
      passed)
        ((passed_tests++))
        ;;
      failed)
        ((failed_tests++))
        ;;
    esac

    local cycle_end_time
    cycle_end_time=$(date +%s)
    cycle_time=$((cycle_end_time - cycle_start_time))

    printf -- "power cycle test record : "
    printf -- "%s, " \
      "${total_tests}" \
      "${test_result}" \
      "${test_type}" \
      "${test_time}" \
      "${cycle_time}" \
      "${passed_tests}" \
      "${failed_tests}"
    printf -- "\n"

    [ "passed" = "${test_result}" ] && failed_test_recovery
}

wait_for_board_to_boot()
{
  export target_ipaddr
  timeout 3m bash -c \
    "until printf \"42\" | nc -w 1 -q 1 ${target_ipaddr} 65432 > /dev/null 2>&1 ; do sleep 1; done"
}

power_cycle_board()
{
    export relay_ipaddr
    on_url="http://${relay_ipaddr}/k01=00"
    off_url="http://${relay_ipaddr}/k01=01"
    curl --silent --show-error "${off_url}" > /dev/null
    sleep 3
    curl --silent --show-error "${on_url}" > /dev/null
    sleep 1
}

reboot_board()
{
  #Note: this may need to be modifed to pass password in
  ssh -n -T "root@${target_ipaddr}" '/sbin/reboot && exit' || return
  #delay to allow CCC to restart
  sleep 5
}

main()
{
    printf -- "power cycle test starting - file: %s target: %s relay: %s\n" \
      "${file_name}" \
      "${target_ipaddr}" \
      "${relay_ipaddr}"

    check_file "${file_name}"

    run_test "${file_name}"
}

main "${@}"
